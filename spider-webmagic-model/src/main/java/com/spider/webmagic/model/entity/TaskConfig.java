package com.spider.webmagic.model.entity;

import java.io.Serializable;

/**
 * 采集任务配置信息
 * @craete by double  2017年7月3日
 */
public class TaskConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	public int tcid;
	
	public String name;

	public String url;

	public boolean drill;

	public String param;

	public String method;

	public String header;

	public int getTcid() {
		return tcid;
	}

	public void setTcid(int tcid) {
		this.tcid = tcid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isDrill() {
		return drill;
	}

	public void setDrill(boolean drill) {
		this.drill = drill;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

}
