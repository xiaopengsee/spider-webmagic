package com.spider.webmagic.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 童游网
 * 
 * @craete by double 2017年6月30日
 */
public class TongYou implements Serializable {

	private static final long serialVersionUID = 1L;

	public String address;		// 地址
	public String title;		// 标题
	public Date time;			// 时间
	public double price;		// 价钱
	public String status;		// 所属 周边游
	public List<String> tags;	// 标签

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

}
