package com.spider.webmagic.model.entity;

import java.io.Serializable;

/**
 * 字段信息
 * @craete by double  2017年7月3日
 */
public class Fields implements Serializable {

	private static final long serialVersionUID = 1L;

	public String fid;			// 关联实体id
	
	public String type;			// 获取方式

	public String name;			// 字段名称

	public String method;		// 匹配方式

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

}
