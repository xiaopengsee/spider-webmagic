package com.spider.webmagic.dao.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.spider.webmagic.model.entity.Fields;
import com.spider.webmagic.model.entity.TaskConfig;

/**
 * 
 * @craete by double  2017年6月30日
 */
public interface URLAddressMapper {

	/**
	 * 插入配置信息
	 * @param config
	 * @return
	 */
////	@Options(useGeneratedKeys = true, keyProperty = "taskconf.tcid")
	@Insert("insert into `taskconfig` (`name`, `url`, `drill`, `param`, `method`, `header`) values (#{name}, #{url}, #{drill}, #{param}, #{method}, #{header})")
	public int addTaskConfig(TaskConfig config);
	
////	@Options(useGeneratedKeys = true, keyProperty = "fields.fid")
	@Insert("insert into `fields` (`fid`, `name`, `type`, `method`) values (#{fid}, #{name}, #{type}, #{method})")
	public int addTaskFields(Fields fields);
	
	// URLConfig采集链接信息表:  url(请求链接)、method(请求方式)、params(请求参数json)、header(请求头)
	@Select("")
	public void selectURLConfig();
	
	// entityMap采集实体关系映射表: name(实体名称)、type(匹配方式)、path(配备内容)、field(字段名称)
	@Select("")
	public void selectEntity();
	
	
}
