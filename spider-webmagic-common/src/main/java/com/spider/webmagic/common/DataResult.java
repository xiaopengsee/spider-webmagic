package com.spider.webmagic.common;

public class DataResult<T> {

	/**
	 * success = false
	 */
	public DataResult() {
	}

	/**
	 * success = true
	 * @param data
	 */
	public DataResult(T data) {
		this.data = data;
		this.success = true;
	}

	/**
	 * success = false
	 * @param errorCode
	 * @param errorDesc
	 */
	public DataResult(String errorCode, String errorDesc) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
		this.success = false;
	}

	/**
	 * success = false
	 * @param errorCode
	 * @param errorDesc
	 * @param elapsedMilliseconds
	 */
	public DataResult(String errorCode, String errorDesc, long elapsedMilliseconds) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
		this.success = false;
		this.elapsedMilliseconds = elapsedMilliseconds;
	}

	/**
	 * 是否处理成功
	 */
	private boolean success;

	/**
	 * 返回的数据
	 */
	private T data;

	/**
	 * 错误代码
	 */
	private String errorCode;

	/**
	 * 错误描述
	 */
	private String errorDesc;

	/**
	 * 处理耗时(毫秒)
	 */
	private long elapsedMilliseconds;

	public boolean isSuccess() {
		return success;
	}

	@Deprecated
	public void setIsSuccess(boolean isSuccess) {
		this.success = isSuccess;
	}

	public DataResult<T> setSuccess(boolean success) {
		this.success = success;
		return this;
	}

	public T getData() {
		return data;
	}

	public DataResult<T> setData(T data) {
		this.data = data;
		return this;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public DataResult<T> setErrorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public DataResult<T> setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
		return this;
	}

	public long getElapsedMilliseconds() {
		return elapsedMilliseconds;
	}

	public DataResult<T> setElapsedMilliseconds(long elapsedMilliseconds) {
		this.elapsedMilliseconds = elapsedMilliseconds;
		return this;
	}

	@Override
	public String toString() {
		return "DataResult{" + "success=" + success + ", data=" + data + ", errorCode='" + errorCode + '\''
				+ ", errorDesc='" + errorDesc + '\'' + ", elapsedMilliseconds=" + elapsedMilliseconds + '}';
	}

}
