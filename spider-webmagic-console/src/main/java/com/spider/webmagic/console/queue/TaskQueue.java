package com.spider.webmagic.console.queue;

import java.util.concurrent.BlockingQueue;

import org.eclipse.jetty.util.BlockingArrayQueue;

import com.spider.webmagic.model.entity.SpiderTask;

/**
 * 该类为放置需要执行的任务的队列，还在思考是否使用外部的消息队列
 * @craete by double  2017年7月3日
 */
public class TaskQueue {

	/**
	 * 存放采集任务
	 */
	BlockingQueue<SpiderTask> queue = new BlockingArrayQueue<SpiderTask>();
	
	/**
	 * 获取采集任务
	 * @return
	 */
	public synchronized SpiderTask getTask() {
		if (queue.size() > 0) {
			return queue.poll();
		}
		return null;
	}
	
	/**
	 * 存放采集任务
	 * @param task
	 */
	public synchronized boolean setTask(SpiderTask task) {
		return queue.add(task);
	}
	
}
