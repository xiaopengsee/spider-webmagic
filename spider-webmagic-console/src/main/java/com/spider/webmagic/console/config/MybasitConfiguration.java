package com.spider.webmagic.console.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import tk.mybatis.mapper.mapperhelper.MapperInterceptor;

@Configuration
@MapperScan({ "com.spider.webmagic.dao.mapper" })
public class MybasitConfiguration {

	@Autowired DataSource dataSource;
	
	@Bean
	public SqlSessionFactory sqlSessionFactoryBean() throws Exception {

		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);
	
		sqlSessionFactoryBean.setTypeAliasesPackage("com.spider.webmagic.model.entity");
		MapperInterceptor tkMapperInterceptor = new MapperInterceptor();
		Properties tkProp = new Properties();
		tkProp.setProperty("IDENTITY", "MYSQL");
		tkProp.setProperty("notEmpty", "true");
		tkMapperInterceptor.setProperties(tkProp);
		Interceptor[] interceptors = new Interceptor[1];
		interceptors[0] = tkMapperInterceptor;

		sqlSessionFactoryBean.setPlugins(interceptors);
		
		return sqlSessionFactoryBean.getObject();
	}
	
	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource);
	}
	
}
