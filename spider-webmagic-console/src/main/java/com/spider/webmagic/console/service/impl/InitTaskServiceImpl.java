package com.spider.webmagic.console.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spider.webmagic.console.service.InitTaskService;
import com.spider.webmagic.dao.mapper.URLAddressMapper;
import com.spider.webmagic.model.entity.Fields;
import com.spider.webmagic.model.entity.TaskConfig;

/**
 * 存储采集任务(任务配置和任务状态)、任务具体执行和具体存储
 * @craete by double  2017年7月3日
 */
@Service("initTaskService")
public class InitTaskServiceImpl implements InitTaskService {

	@Autowired URLAddressMapper addressMapper;
	
	/**
	 * 添加采集配置
	 */
	public boolean addTaskConfig(TaskConfig config) {
		int num = addressMapper.addTaskConfig(config);
		if (num > 0)
			return true;
		return false;
	}

	/**
	 * 添加实体
	 * @throws Exception 
	 */
	public boolean addTaskField(List<Fields> fields) throws Exception {
		String fid = UUID.randomUUID().toString();
		try {
			for (Fields field : fields) {
				field.setFid(fid);
				addressMapper.addTaskFields(field);
			}
		} catch (Exception e) {
			throw new Exception(e.toString());
		}
		return true;
	}

}
