package com.spider.webmagic.console.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.spider.webmagic.console.queue.TaskQueue;

@Configuration
public class QueueConfiguration {

	@Bean
	public TaskQueue getTaskQueue() {
		return new TaskQueue();
	}
	
}
