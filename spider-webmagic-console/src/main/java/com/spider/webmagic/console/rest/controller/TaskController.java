package com.spider.webmagic.console.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spider.webmagic.common.DataResult;
import com.spider.webmagic.console.service.InitTaskService;
import com.spider.webmagic.model.entity.Fields;
import com.spider.webmagic.model.entity.TaskConfig;

/**
 * 该类主要是存储页面配置采集任务
 * @craete by double  2017年7月3日
 */
@RestController
@RequestMapping("/task")
public class TaskController {

	@Autowired InitTaskService initTask;
	
	/**
	 * 添加任务配置
	 * config:
	 * 		name		任务名称
	 * 		url			任务url
	 * 		drill		是否发现url
	 * 		param		请求参数
	 * 		method		请求方式
	 * 		header		请求头信息
	 * 		等
	 */
	@ResponseBody
	@RequestMapping(value = "conf", method = RequestMethod.POST)
	public DataResult<Boolean> addTaskConfig(@RequestBody TaskConfig config) {
		// TODO 初始化状态都是不采集
		return new DataResult<Boolean>(initTask.addTaskConfig(config));
	}
	
	/**
	 * 添加采集的字段
	 * fields:
	 * 		{"type":"使用的匹配方式", "name":"字段名称", "method":"具体匹配路径"}
	 */
	@ResponseBody
	@RequestMapping(value = "fields", method = RequestMethod.POST)
	public DataResult<Boolean> addTaskFields(@RequestBody List<Fields> fields) {
		try {
			return new DataResult<Boolean>(initTask.addTaskField(fields));
		} catch (Exception e) {
			return new DataResult<Boolean>("500", e.toString());
		}
	}
	
	/**
	 * 指定存储的方式
	 * 		可以直接存储，可以转发出去
	 * 		要配置存储方式和发送方式
	 */
	@ResponseBody
	@RequestMapping(value = "stored", method = RequestMethod.POST)
	public DataResult<Boolean> addTaskStored() {
		// TODO 有待考虑如何存储
		return new DataResult<Boolean>();
	}
	
	/**
	 * 修改任务状态，只有测试通过的方可修改状态
	 */
	@ResponseBody
	@RequestMapping(value = "edit", method = RequestMethod.POST)
	public DataResult<Boolean> editTaskStatus() {
		
		
		return new DataResult<Boolean>();
	}
	
}
