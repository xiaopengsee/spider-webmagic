package com.spider.webmagic.console.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试配置的任务是否可以获取到数据
 * @craete by double  2017年7月3日
 */
@RestController
@RequestMapping("check")
public class CheckController {

	/**
	 * 测试配置的采集任务是否可以正确获取到数据
	 */
	@ResponseBody
	@RequestMapping(value = "test", method = RequestMethod.GET)
	public void checkTask() {
		// TODO 直接发送组装好的采集任务，返回采集数据
		
		
	}
	
}
