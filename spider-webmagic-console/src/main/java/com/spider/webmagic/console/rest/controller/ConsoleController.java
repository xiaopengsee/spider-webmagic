package com.spider.webmagic.console.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spider.webmagic.common.DataResult;
import com.spider.webmagic.console.queue.TaskQueue;
import com.spider.webmagic.model.entity.SpiderTask;

/**
 * 控制中心（控制任务是否开启，任务的完成状况）这个为task调用
 * @craete by double  2017年7月3日
 */
@RestController
@RequestMapping("v1")
public class ConsoleController {

	@Autowired
	TaskQueue queue;
	
	/**
	 * 从队列中获取任务
	 */
	@ResponseBody
	@RequestMapping(value = "get", method = RequestMethod.GET)
	public DataResult<SpiderTask> getTask() {
		return new DataResult<SpiderTask>(queue.getTask());
	}
	
	/**
	 * 获取所有task的存活状态
	 */
	@ResponseBody
	@RequestMapping(value = "status", method = RequestMethod.POST)
	public void collectStatus() {
		// 获取到当前任意一个task在执行的任务
	}
	
	/**
	 * 获取采集的数据量
	 */
	@ResponseBody
	@RequestMapping(value = "s", method = RequestMethod.POST)
	public void processorStatus() {
		
	}
	
	/**
	 * 获取到具体任务状态
	 */
	@ResponseBody
	@RequestMapping(value = "a", method = RequestMethod.POST)
	public void getProcessorTask() {
		
	}
	
}
