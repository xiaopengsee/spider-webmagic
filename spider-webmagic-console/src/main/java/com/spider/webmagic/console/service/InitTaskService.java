package com.spider.webmagic.console.service;

import java.util.List;

import com.spider.webmagic.model.entity.Fields;
import com.spider.webmagic.model.entity.TaskConfig;

/**
 * 初始任务配置
 * @craete by double  2017年7月3日
 */
public interface InitTaskService {

	/**
	 * 添加采集配置
	 * @param config
	 * @return
	 */
	public boolean addTaskConfig(TaskConfig config);
	
	/**
	 * 添加采集字段
	 * @param fields
	 * @return
	 */
	public boolean addTaskField(List<Fields> fields) throws Exception;
	
}
