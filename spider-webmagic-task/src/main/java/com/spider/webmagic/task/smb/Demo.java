package com.spider.webmagic.task.smb;

import java.net.MalformedURLException;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

public class Demo {

	public static void main(String[] args) throws MalformedURLException, SmbException {
		
		SmbFile remote = new SmbFile("smb://192.168.1.122/share/zhoucheng/");
		String[] files = remote.list();
		for (String file : files) {
			System.out.println(file);
		}
		
	}
	
}
