package com.spider.webmagic.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.spider.webmagic.listener.SpiderTaskListener;

/**
 * 启动类
 * @craete by double  2017年6月30日
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		
		SpringApplication springApplication = new SpringApplication(Application.class);
		springApplication.setWebEnvironment(false);
		springApplication.addListeners(new SpiderTaskListener());
		springApplication.run(args);
	}
	
}
