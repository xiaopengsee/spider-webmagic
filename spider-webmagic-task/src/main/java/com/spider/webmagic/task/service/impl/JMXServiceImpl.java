package com.spider.webmagic.task.service.impl;

import org.springframework.stereotype.Service;

import com.spider.webmagic.task.service.JMXService;

/**
 * 获取具体采集任务的jmx信息
 * @craete by double  2017年7月3日
 */
@Service("jMXService")
public class JMXServiceImpl implements JMXService {

}
