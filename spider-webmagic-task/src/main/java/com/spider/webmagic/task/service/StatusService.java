package com.spider.webmagic.task.service;

public interface StatusService {

	/**
	 * 发送集群心跳
	 */
	public void sendHeartbeat();
	
	/**
	 * 发送采集的具体情况
	 */
	public void sendProcessorJMXStatus();
	
	/**
	 * 发送采集的具体数量
	 */
	public void sendProcessorCount();
	
}
