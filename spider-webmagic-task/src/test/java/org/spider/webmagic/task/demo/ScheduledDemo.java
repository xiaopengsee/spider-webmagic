package org.spider.webmagic.task.demo;

import us.codecraft.webmagic.Spider;

/**
 * 定时任务模块
 * @craete by double  2017年6月29日
 */
public class ScheduledDemo {

	public void demoScheduled() {
        System.out.println("----开始执行模板定时任务");
        Spider spider = Spider.create(new ProcessorDemo());
        spider.addUrl("http://www.jianshu.com");
        spider.addPipeline(new PipelineDemo());
        spider.thread(5);
        spider.setExitWhenComplete(true);
        spider.start();
        spider.stop();
    }
	
}
