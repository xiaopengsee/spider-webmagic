package org.spider.webmagic.task.comment;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.example.GithubRepo;
import us.codecraft.webmagic.model.ConsolePageModelPipeline;
import us.codecraft.webmagic.model.OOSpider;
import us.codecraft.webmagic.model.annotation.ExtractBy;
import us.codecraft.webmagic.model.annotation.ExtractByUrl;
import us.codecraft.webmagic.model.annotation.HelpUrl;
import us.codecraft.webmagic.model.annotation.TargetUrl;

// 最终需要抓取的url
@TargetUrl("https://github.com/\\w+/\\w+")
// 发现最终需要抓取的url
@HelpUrl("https://github.com/\\w+")

// @ExtractBy这个注解是指抽取一个区域的信息作为这个类信息   @ExtractBy(value = "//ul[@id=\"promos_list2\"]/li",multi = true)
public class CommentSpider {

	// 数据的抓取规则 notnull属性是抓取是否非空
	@ExtractBy(value = "//h1[@class='entry-title public']/strong/a/text()", notNull = true)
    private String name;
	
	// 抓取结果类型默认转换，只有支持部分
    @ExtractByUrl("https://github\\.com/(\\w+)/.*")
    private String author;

    // @Formatter("yyyy-MM-dd HH:mm") 该注解可以转换时间格式
    @ExtractBy("//div[@id='readme']/tidyText()")
    private String readme;
    
    public static void main(String[] args) {
        OOSpider.create(Site.me().setSleepTime(1000)
                , new ConsolePageModelPipeline(), GithubRepo.class)
                .addUrl("https://github.com/code4craft").thread(5).run();
    }
	
}
