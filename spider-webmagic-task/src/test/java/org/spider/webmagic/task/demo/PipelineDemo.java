package org.spider.webmagic.task.demo;

import java.util.Map;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * 入库模块
 * 
 * @craete by double 2017年6月29日
 */
public class PipelineDemo implements Pipeline {

	public void process(ResultItems resultItems, Task task) {
		for (Map.Entry<String, Object> entry : resultItems.getAll().entrySet()) {
			System.out.println(entry.getKey()); // key
			System.out.println(entry.getValue().toString()); // 实体类
			System.out.println("===================");
		}
	}

}
