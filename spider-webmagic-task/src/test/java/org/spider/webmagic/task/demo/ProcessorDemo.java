package org.spider.webmagic.task.demo;

import java.io.Serializable;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

/**
 * 页面爬取(爬取模块)
 * @craete by double  2017年6月29日
 */
public class ProcessorDemo implements PageProcessor {

	class News implements Serializable {
		private static final long serialVersionUID = 1L;
		public String title;
		public String auth;
		public String link;
		public String content;
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuth() {
			return auth;
		}
		public void setAuth(String auth) {
			this.auth = auth;
		}
		public String getLink() {
			return link;
		}
		public void setLink(String link) {
			this.link = link;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		@Override
		public String toString() {
			return "News [title=" + title + ", auth=" + auth + ", link=" + link + ", content=" + content + "]";
		}
	}
	
	private static final String URL = "http://www.jianshu.com"; // 采集链接
	
	// 模拟浏览器信息
	private Site site = Site.me();
//			.setDomain("jianshu.com")
//			.setTimeOut(1000)
//			.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
	
	public Site getSite() {
		return this.site;
	}

	public void process(Page page) {
		if (page.getUrl().regex(URL).match()) {
            List<Selectable> list = page.getHtml().xpath("//ul[@class='note-list']/li").nodes();
            for (Selectable s : list) {
                String title = s.xpath("//div[@class='content']/a[@class='title']/text()").toString();
                String auth = s.xpath("//div[@class='content']/div[@class='author']/div[@class='name']/a/text()").toString();
                String link = s.xpath("//div[@class='content']/a[@class='title']").links().toString();
                String content = s.xpath("//div[@class='content']/p[@class='abstract']/text()").toString();
                
                News news = new News();
                news.setTitle(title);
                news.setAuth(auth);
                news.setLink(link);
                news.setContent(content);
                page.putField("news" + title, news); // key不可重复
            }
        }
	}
	
	public static void main(String[] args) {
		
		Spider spider = Spider.create(new ProcessorDemo());
        spider.addUrl("http://www.jianshu.com");
        spider.addPipeline(new PipelineDemo());
        spider.thread(1);
        spider.setExitWhenComplete(true);
        spider.start();
		
	}
	
}
