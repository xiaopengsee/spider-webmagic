package org.spider.webmagic.task.demo;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class URLFindProcessorDemo implements PageProcessor {

	private Site site = Site.me().setTimeOut(1000).setRetryTimes(3).setSleepTime(100);
	private static int count = 0;
	
	public void process(Page page) {
		
		if (page.getUrl().regex("http://www.cnblogs.com/[a-z 0-9 -]+/p/[0-9]{7}.html").match()) {
			page.addTargetRequests(page.getHtml().xpath("//*[@id=\"post_list\"]/div/div[@class='post_item_body']/h3/a/@href").all());
		} else {
			System.out.println(page.getHtml().xpath("//*[@class=\"titlelnk\"]/text()").get());
			count++;
		}
		
	}

	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		
		System.out.println("init");
		long startTime = System.currentTimeMillis();
		Spider.create(new URLFindProcessorDemo()).addUrl("https://www.cnblogs.com/").thread(5).run();
		long endTime = System.currentTimeMillis();
		System.out.println("end. use time:" + ((endTime - startTime) / 1000) + "s, count:" + count);
		
	}
	
}
