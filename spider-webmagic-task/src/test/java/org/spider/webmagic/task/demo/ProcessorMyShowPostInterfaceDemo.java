package org.spider.webmagic.task.demo;

import java.io.UnsupportedEncodingException;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.utils.HttpConstant;

public class ProcessorMyShowPostInterfaceDemo implements PageProcessor {

	private Site site = Site.me().setRetryTimes(3).setTimeOut(1000).addHeader("Content-Type", "application/json");
	private static String encoding = "utf-8";
	private static String json = "{\"len\": 1, \"cameras\": \"27\", \"start\": \"2017-06-30 14:00:11\", \"end\": \"2017-06-30 15:00:11\", \"names\": [\"奥迪A6L\"]}";
	
	public void process(Page page) {
		System.out.println(page.getJson().get());
	}

	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		Request request = new Request("http://show.urundp.com:6002/passengerrest/playback/back");
		request.setMethod(HttpConstant.Method.POST);
		try {
			request.setRequestBody(HttpRequestBody.json(json, encoding));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Spider.create(new ProcessorMyShowPostInterfaceDemo()).addRequest(request).thread(1).start();
		
	}
	
}
